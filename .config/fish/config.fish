# Variables
set -gx GUILE_AUTO_COMPILE 0
set -gx EDITOR vim
set -gx VISUAL vim
set -gx GTK_THEME "Adwaita:dark"
set -gx BROWSER qutebrowser

# Set vim key bindings
fish_vi_key_bindings

# 'Aliases'
function l
    ls -lh
end

function rak
    racket -i -p dyoo/simply-scheme -l xrepl
end

# Be silent
function fish_greeting
end
